exports = module.exports = {
    pushNotificationToClient,
}

const admin = require("firebase-admin");

const serviceAccount = require("../configs/firebase-key.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://ananto-6293c.firebaseio.com"
});

/**
 *
 * Push notification request to firebase
 * @function
 * @param {object} notificationObject - time parsed JSON object
 * @param {requestCallback} callback
 */

function pushNotificationToClient(notificationObject, callback) {
  return admin.messaging().send({
    notification: {
      title: "Ananto",
      body: notificationObject.message
    },
    token: notificationObject.uid,
  }).then(response => {
    return callback(null, response);
  }).catch(error => {
    return callback(error);
  })
}