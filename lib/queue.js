exports = module.exports = {
    enqueueNotification,
}

const debug = require("debug")("queue");

const Queue = require("bee-queue");

const config = require("../configs/service");
const firebase = require("./firebase");

const notificationQueue = new Queue("notifications", {
    redis: config.redis.dev,
    activateDelayedJobs: true,
    removeOnSuccess: true,
    removeOnFailure: true,
});

/**
 *
 * Enqueue incoming messages from the backend
 * @function
 * @param {object} dataObject - JSON object received from the backend
 * @param {requestCallback} callback
 */

function enqueueNotification(dataObject, callback) {
    const newNotifcation = notificationQueue.createJob(dataObject);

    if (dataObject.level !== null) newNotifcation.delayUntil(dataObject.level);

    return newNotifcation.retries(2).save().then(job => {
        debug("saving job to queue");
        return callback(null, job.id);
    }).catch(error => {
        debug("error in saving job to queue");
        return callback(error);
    });
}

notificationQueue.ready(() => {
    debug("queue is ready to receive jobs");
});

notificationQueue.process((job, done) => {
    debug("processing jobs in notifications queue");

    return firebase.pushNotificationToClient(job.data, (error, pushStatus) => {
        if (error) {
            debug("error pushing notification to firebase");
            return done(error);
        }

        debug("notification pushed to firebase");
        return done(null, pushStatus);
    });
});