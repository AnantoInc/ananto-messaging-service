exports = module.exports = {
    optimumTime
}

const debug = require("debug")("utils");

const spacetime = require("spacetime");

const config = require("../configs/service");
const queue = require("./queue");

/**
 *
 * Enqueue incoming messages from the backend
 * @function
 * @param {object} messageObject - JSON object received from the backend
 * @param {requestCallback} callback
 */

function optimumTime(messageObject = {}, callback) {
    messageObject = JSON.parse(messageObject);

    let currentTime = spacetime.now(config.timezone);

    switch (messageObject.level) {
        case 1:
            currentTime = currentTime.add(getRandomInt(6, 8), "hour");
            break;
        case 2:
            currentTime = currentTime.add(getRandomInt(4, 6), "hour");
            break;
        case 3:
            currentTime = currentTime.add(getRandomInt(2, 4), "hour");
            break;
        case 4:
            currentTime = currentTime.add(getRandomInt(5, 120), "minute");
            break;
        case 5:
            messageObject.level = null;
            break;
        default:
            messageObject.level = null;
            break;
    }

    const currentHour = currentTime.hour();

    if (currentHour >= 0 && currentHour <= 7) currentTime = currentTime.add(7 - currentHour, "hour");
    if (messageObject.level !== null) messageObject.level = currentTime.epoch;

    debug(messageObject);

    return queue.enqueueNotification(messageObject, (error, ctx) => {
        if (error) {
            debug(error);
            return callback(error);
        }

        return callback(null, ctx);
    });
}

function getRandomInt(min, max) {
    min = Math.floor(min);
    max = Math.ceil(max);
    return Math.floor(Math.random() * (max - min)) + min;
}