/**
 * This is a JSON specification of how messages from the backend should be sent to the noticication service
 *
 * This file is NOT a module and should not be imported anywhere in the project
 *
 */

const messageSpecObject = {
    /**
     * message
     *
     * The intended message to be sent to the android user
     */

    "message": "",

    /**
     * uid
     *
     * The firebase UID of the intended recepient
     */

    "uid": "",

    /**
     * level
     *
     * The notification level of the message
     *
     * Should be between 1 and 5
     *  :- 1 = lowest urgency (within 8 hrs)
     *     2 = .. (within 6 hrs)
     *     3 = .. (within 4 hrs)
     *     4 = .. (within 2 hrs)
     *     5 = instant notification
     *
     * Notifications should NOT be sent between:
     *     0000 - 0700 hrs IST
     *     * instant notifications are an exception
     *
     * Time to be choosen can be randomized between the limits
     *
     * If notification is queued at restricted times, then they should be delivered at earliest as soon as restriction is over
     *
     * Example:
     *
     * Notification with level 4 is queued at 1800 hrs. It should be randomly delivered between (2000 - 0000)
     */

     "level": "",
}