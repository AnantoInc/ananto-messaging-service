const debug = require("debug")("service");

const WebSocket = require("ws");

const config = require("./configs/service")
const utils = require("./lib/utils");

const wss = new WebSocket.Server({ port: config.server.port });

wss.on("connection", ws => {
    debug("wss connection opened");
    ws.send("successfully connected to notification service");

    ws.on("message", msg=> {
        debug("wss received message from client");
        return utils.optimumTime(msg, (error, jobId) => {
            if (error) {
                debug("error enqueing job");
                return ws.send("error enqueieng job");
            }

            debug(`successfully queued job ${jobId}`);
            return ws.send(`successfully queued job ${jobId}`);
        });
    });
});

wss.on("error", error => {
    debug("error in wss");
    debug(error);
    return wss.close(() => {
        return debug("closing wss");
    });
});